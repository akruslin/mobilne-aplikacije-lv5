import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Food extends StatelessWidget{
  String imageSource;
  String nameOfDish;
  String ingridients;
  String description;

  Food(this.imageSource, this.nameOfDish, this.ingridients, this.description);

  String get getImageSource => imageSource;
  String get getNameOfDish => nameOfDish;
  String get getIngridients => ingridients;

  @override
  Widget build (BuildContext context)
  {
    return Container(
      width: double.infinity,
      height: 100,
      margin: EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Image.asset(imageSource, alignment: Alignment.bottomRight,),
            flex: 1,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  nameOfDish,
                  style: TextStyle(fontSize: 20),
                  textAlign: TextAlign.center,
                ),
                Text(
                  ingridients,
                  style: TextStyle(fontSize: 12),
                ),
                Text(
                  description,
                  style: TextStyle(fontSize: 15,),textAlign: TextAlign.center,
                ),
              ],
            ),
            flex:2,
            )
        ],
      ),
    );
  }
}
