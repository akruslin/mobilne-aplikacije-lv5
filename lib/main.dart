import 'package:flutter/material.dart';
import './food.dart';
import 'dart:math';

void main() => runApp(new MaterialApp (home: new MyApp(),));


class MyApp extends StatefulWidget {
   @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State <MyApp>
{
   TextEditingController myController=TextEditingController();
  List<Food> foodWidgets=[];
  int radioValue=0;
  int groupValue=-1;

  void addFood(Food food)
  {
    setState(() {
      foodWidgets.add(food);
    });
  }

  void changeRadioValue(int value)
  {
    setState(() {
      radioValue=value;
      groupValue=value;
    });
  }
  void changeDescription(int radioValue,String textInput)  
  {
    setState(() {
      Food editFoodDescription= Food(foodWidgets[radioValue].getImageSource,foodWidgets[radioValue].getNameOfDish, foodWidgets[radioValue].getIngridients,textInput);
      foodWidgets.removeAt(radioValue);
      foodWidgets.insert(radioValue, editFoodDescription);
      myController.clear();
    });
  }

  @override
  void initState()
  {
    super.initState();
    foodWidgets.add(Food("assets/steak.jpg", "Steak", "Beef,Salt,Butter,Thyme,Black pepper"," Beef steak is a flat cut of beef, with parallel faces spaced to a thickness of 1 to 5 centimetres (1⁄2 to 2 in)..."));
    foodWidgets.add(Food("assets/sushi.jpg", "Sushi", "Rice,Kombu,Wasabi,Soy sauce,Fish","Great for you if you want to live long."));
    foodWidgets.add(Food("assets/sarma.jpg", "Sarma", "Sour cabbage,Onion,Mixed meat","Eat it if you want to make your grandma happy."));

  }

  final random= new Random();

  List <String> inspirationList=["If more of us valued food and cheer and song above hoarded gold, it would be a merrier world.","I love you like a fat kid loves cake!","After a good dinner one can forgive anybody, even one's own relations.","There is no love sincerer than the love of food."];

  void _showInspirationDialog(String content)
  {
    showDialog(
      context: context,
      builder:  (context)
      {
        return AlertDialog(
          title: Text("Inspiration"),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              onPressed: (){
                Navigator.pop(context);
              },
              child:Text("Close"),
            )
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context)
  {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Food menu"),

        ),
        body: ListView(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Card(
                child: RaisedButton(
                  child: Text("Inspiration"),
                  onPressed: () => _showInspirationDialog(inspirationList[random.nextInt(inspirationList.length)]),
                ),
              ),
            ),
            ...foodWidgets,
            TextField(
              decoration: InputDecoration(hintText: "Enter new food description"), textAlign: TextAlign.center,
              controller: myController,
            ),
            RaisedButton(
              child: Text("Edit food description"),
              onPressed: () => changeDescription(radioValue,myController.text),
            ),
            Row(
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: groupValue,
                  onChanged: (value) => changeRadioValue(value),
                  activeColor: Colors.blue,
                ),
                Text("First dish"),
                Radio(
                  value: 1,
                  groupValue: groupValue,
                  onChanged: (value) =>  changeRadioValue(value),
                  activeColor: Colors.blue,
                ),
                Text("Second dish"),
                Radio(
                  value: 2,
                  groupValue: groupValue,
                  onChanged: (value) =>  changeRadioValue(value),
                  activeColor: Colors.blue,
                ),
                Text("Third dish")
          ],
        ),
          ]
      ),
      ),
    );
  }
}